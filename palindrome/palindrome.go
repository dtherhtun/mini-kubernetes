package palindrome

type PALINDROME struct{}

func (p *PALINDROME) IsPaLindRome(text string) bool {
	var result bool
	for i := 0; i < len(text)/2; i++ {
		if text[i] == text[len(text)-i-1] {
			result = true
		} else {
			result = false
		}
	}
	return result
}
