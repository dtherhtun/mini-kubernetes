package rot13

type CRYPTO struct{}

func (e *CRYPTO) Crypto(c string) string {
	var result string
	for _, v := range []byte(c) {
		result += rot13(v)
	}
	return result
}

func rot13(c byte) string {
	var result byte
	var asciiLowercase = []byte("abcdefghijklmnopqrstuvwxyz")
	var asciiUppercase = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

	if (c >= asciiLowercase[0] && c <= asciiLowercase[12]) || (c >= asciiUppercase[0] && c <= asciiUppercase[12]) {
		result = c + 13
	} else if c == 32 {
		result = c
	} else {
		result = c - 13
	}
	return string([]byte{result})
}
