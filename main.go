package main

import (
	"fmt"
	"os"

	"gitlab.com/dtherhtun/mini-kubernetes/docker"
	"gitlab.com/dtherhtun/mini-kubernetes/kubelet"
	"gitlab.com/dtherhtun/mini-kubernetes/palindrome"
	"gitlab.com/dtherhtun/mini-kubernetes/rot13"
)

func main() {
	cmd := os.Args[2]
	kubelet := kubelet.NewKubeLetUseCase(new(docker.CONTAINER), new(docker.HEALTH), new(rot13.CRYPTO), new(palindrome.PALINDROME))
	fmt.Println(cmd)

	switch cmd {
	case "pod":
		fmt.Println(kubelet.GetPod())
	case "status":
		fmt.Println(kubelet.GetStatus())
	case "encrypt":
		fmt.Println(kubelet.Encrypt(os.Args[3]))
	case "decrypt":
		fmt.Println(kubelet.Decrypt(os.Args[3]))
	case "palindrome":
		fmt.Println(kubelet.IsPaLindRome(os.Args[3]))
	}
}
