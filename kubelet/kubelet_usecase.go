package kubelet

import (
	"gitlab.com/dtherhtun/mini-kubernetes/docker"
	"gitlab.com/dtherhtun/mini-kubernetes/palindrome"
	"gitlab.com/dtherhtun/mini-kubernetes/rot13"
)

type kubeletusecase struct {
	container  *docker.CONTAINER
	health     *docker.HEALTH
	crypto     *rot13.CRYPTO
	palindrome *palindrome.PALINDROME
}

type KubeLetUseCase interface {
	GetPod() int
	GetStatus() string
	Encrypt(secret string) string
	Decrypt(secret string) string
	IsPaLindRome(text string) bool
}

func NewKubeLetUseCase(c *docker.CONTAINER, h *docker.HEALTH, e *rot13.CRYPTO, p *palindrome.PALINDROME) KubeLetUseCase {
	return &kubeletusecase{
		container:  c,
		health:     h,
		crypto:     e,
		palindrome: p,
	}
}

func (c *kubeletusecase) GetPod() int {
	return c.container.GetContainer()
}

func (h *kubeletusecase) GetStatus() string {
	return h.health.GetHealth()
}

func (e *kubeletusecase) Encrypt(secret string) string {
	return e.crypto.Crypto(secret)
}

func (e *kubeletusecase) Decrypt(secret string) string {
	return e.crypto.Crypto(secret)
}

func (p *kubeletusecase) IsPaLindRome(text string) bool {
	return p.palindrome.IsPaLindRome(text)
}
